package com.integration.demo.controller;

import com.integration.demo.service.IntegrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is the integration controller class
 */
@RestController
@RequestMapping("")
public class IntegrationController {

    @Autowired
    private IntegrationService integrationService;

    @PostMapping("/envelope")
    public ResponseEntity<?> integrateEntityFile(@RequestBody String entityType) {
        integrationService.integrateEntity(entityType);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

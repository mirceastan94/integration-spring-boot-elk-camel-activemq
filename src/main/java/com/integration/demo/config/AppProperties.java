package com.integration.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * This is the application properties values class
 */
@Configuration
@PropertySource("application.properties")
public class AppProperties {

    @Value("${spring.activemq.user}")
    private String activeMqUser;

    @Value("${spring.activemq.password}")
    private String activeMqPassword;

    @Value("${spring.activemq.broker-url}")
    private String activeMqBrokerURL;

    public String getActiveMqUser() {
        return activeMqUser;
    }

    public String getActiveMqPassword() {
        return activeMqPassword;
    }

    public String getActiveMqBrokerURL() {
        return activeMqBrokerURL;
    }

}

package com.integration.demo.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

/**
 * This is the ActiveMQ configuration class
 */
@Configuration
public class ActiveMQConfig {

    @Autowired
    private AppProperties appProperties;

    @Bean
    public ConnectionFactory jmsConnectionFactory() {
        return new ActiveMQConnectionFactory(appProperties.getActiveMqUser(), appProperties.getActiveMqPassword(), appProperties.getActiveMqBrokerURL());
    }

    @Bean
    public Connection jmsConnection() throws JMSException {
        final Connection connection = jmsConnectionFactory().createConnection();
        connection.start();
        return connection;
    }

}

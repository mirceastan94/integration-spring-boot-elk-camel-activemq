package com.integration.demo.repository;

import com.integration.demo.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

/**
 * This is the skill repository interface
 */
public interface SkillRepository extends JpaRepository<Skill, UUID> {
    Optional<Skill> findByTechnology(String technology);
}


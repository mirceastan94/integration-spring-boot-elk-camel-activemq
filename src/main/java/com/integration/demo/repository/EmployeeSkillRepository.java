package com.integration.demo.repository;

import com.integration.demo.entity.Employee;
import com.integration.demo.entity.EmployeeSkill;
import com.integration.demo.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

/**
 * This is the employee skill repository interface
 */
public interface EmployeeSkillRepository extends JpaRepository<EmployeeSkill, UUID> {
    Optional<EmployeeSkill> findByEmployeeAndSkill(Employee employee, Skill skill);
}

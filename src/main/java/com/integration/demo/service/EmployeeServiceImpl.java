package com.integration.demo.service;

import com.integration.demo.dao.EmployeeDaoImpl;
import com.integration.demo.jms.jaxb.EmployeeJAXB;
import com.integration.demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This is the employee service implementation
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeDaoImpl employeeDao;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void integrateEmployee(EmployeeJAXB employee) {
        employeeDao.insertOrUpdate(employee);
    }
}

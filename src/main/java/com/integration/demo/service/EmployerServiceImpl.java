package com.integration.demo.service;

import com.integration.demo.entity.Employer;
import com.integration.demo.entity.spec.Agency;
import com.integration.demo.jms.jaxb.EmployerJAXB;
import com.integration.demo.repository.EmployerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This is the employer service implementation
 */
@Service
public class EmployerServiceImpl implements EmployerService {

    @Autowired
    private EmployerRepository employerRepository;

    @Override
    public void integrateEmployer(EmployerJAXB employerJAXB) {
        final Employer employer = employerRepository.findByName(employerJAXB.getName()).orElse(new Employer());
        employer.setName(employerJAXB.getName());
        employer.setEmployees(employerJAXB.getEmployees());
        employer.setHeadquarters(employerJAXB.getHeadquarters());
        employerJAXB.getAgencies().forEach(agencyJAXB -> employer.getAgencies().add(Agency.valueOf(agencyJAXB.name())));

        employerRepository.save(employer);
    }

}

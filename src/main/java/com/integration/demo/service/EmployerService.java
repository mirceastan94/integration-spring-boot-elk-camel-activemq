package com.integration.demo.service;

import com.integration.demo.jms.jaxb.EmployerJAXB;

/**
 * This is the employer service interface
 */
public interface EmployerService {

    void integrateEmployer(EmployerJAXB employerJAXB);
}

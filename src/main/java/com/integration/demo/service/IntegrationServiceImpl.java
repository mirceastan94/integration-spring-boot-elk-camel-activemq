package com.integration.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.jms.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.integration.demo.jms.utils.Utils.*;

/**
 * This is the integration service implementation
 */
@Service
public class IntegrationServiceImpl implements IntegrationService {

    @Value("${spring.activemq.broker-url}")
    private String brokerUrl;

    @Autowired
    private Connection connection;

    @Override
    public void integrateEntity(String entityType) {
        try {
            final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            final Destination destination = session.createQueue(INPUT_QUEUE);
            final MessageProducer producer = session.createProducer(destination);
            final String xmlStr = readEnvelopeFromFile(entityType);
            final TextMessage message = session.createTextMessage(xmlStr);
            message.setJMSType(entityType.toLowerCase());
            producer.send(message);
        } catch (JMSException e) {
            // handle exception here with a ControllerAdvice
            e.printStackTrace();
        }
    }

    private String readEnvelopeFromFile(String entityType) {
        try {
            if (entityType.equalsIgnoreCase("employer")){
                return Files.readString(Paths.get(EMPLOYERS_XML_PATH));
            } else {
                return Files.readString(Paths.get(EMPLOYEES_XML_FILE));
            }
        } catch (final IOException ex) {
            throw new RuntimeException("Envelope unmarshalling could not be completed: " + ex.getStackTrace());
        }
    }

}

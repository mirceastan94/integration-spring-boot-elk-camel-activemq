package com.integration.demo.service;

import com.integration.demo.jms.jaxb.EmployeeJAXB;

/**
 * This is the employee service interface
 */
public interface EmployeeService {

    void integrateEmployee(EmployeeJAXB employee);

}

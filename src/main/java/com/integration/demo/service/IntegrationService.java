package com.integration.demo.service;

/**
 * This is the integration service interface
 */
public interface IntegrationService {

    void integrateEntity(String type);

}

package com.integration.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/**
 * This is the spring application main class
 *
 * @author Mircea Stan
 */
@SpringBootApplication
@EnableElasticsearchRepositories
public class IntegrationDemoApp {
    public static void main(String[] args) {
        SpringApplication.run(IntegrationDemoApp.class, args);
    }
}

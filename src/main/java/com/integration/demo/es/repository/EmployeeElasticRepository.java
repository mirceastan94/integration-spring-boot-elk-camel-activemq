package com.integration.demo.es.repository;

import com.integration.demo.es.document.Employee;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * This is the skill elastic index class
 */
@Repository
public interface EmployeeElasticRepository extends ElasticsearchRepository<Employee, String> {
}

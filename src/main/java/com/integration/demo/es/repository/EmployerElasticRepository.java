package com.integration.demo.es.repository;

import com.integration.demo.es.document.Employer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * This is the employer elastic index class
 */
@Repository
public interface EmployerElasticRepository extends ElasticsearchRepository<Employer, String> {
}

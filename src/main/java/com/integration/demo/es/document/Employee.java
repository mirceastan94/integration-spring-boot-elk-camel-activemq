package com.integration.demo.es.document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;

/**
 * This is the employee elastic index class
 */
@Document(indexName = "employeeidx")
@ToString
@Getter
@Setter
public class Employee {

    @Id
    private String id;
    private String agency;
    private List<Skill> skills;
    private String name;
    private String email;
    private int age;


}

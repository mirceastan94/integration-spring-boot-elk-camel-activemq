package com.integration.demo.es.document;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * This is the skill elastic index class
 */
@Document(indexName = "skillIdx")
@Getter
@Setter
public class Skill {

    @Id
    private String id;
    private String technology;
    private String proficiency;
    private boolean main;

}

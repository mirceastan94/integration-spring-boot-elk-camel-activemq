package com.integration.demo.es.document;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;

/**
 * This is the employer elastic index class
 */
@Document(indexName = "employeridx")
@Getter
@Setter
public class Employer {

    @Id
    private String id;
    private String name;
    private String headquarters;
    private int employees;
//    @Field(type = FieldType.Nested)
    private List<String> agencies;

}

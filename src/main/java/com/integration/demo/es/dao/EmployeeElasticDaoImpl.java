package com.integration.demo.es.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.integration.demo.es.document.Employee;
import com.integration.demo.es.document.Skill;
import com.integration.demo.jms.validator.EmployeeValidator;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.integration.demo.jms.utils.Utils.EMPLOYEE_SAMPLE;

/**
 * This is the employee elastic dao implementation
 */
@Component
public class EmployeeElasticDaoImpl implements EmployeeElasticDao {

    @Autowired
    private ElasticsearchRestTemplate template;

    @Autowired
    private RestHighLevelClient restClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TaskExecutor taskExecutor;

    static final Logger logger = LoggerFactory.getLogger(EmployeeElasticDaoImpl.class);

    private static final String INDEX_NAME = "employeesample";
    private static final String INDEX_TYPE = "employee";

    private static final String[] namesArray = {"George", "Michael", "Alex", "Paul", "Sean"};
    private static final String[] agenciesArray = {"Brasov", "Bucharest", "Cluj", "Iasi", "Paris", "Frankfurt"};
    private static final String[] technologiesArray = {"Java", "PostgreSQL", "Maven", "Paul", "Sean"};
    private static final String[] proficiencyArray = {"Low", "Medium", "High"};

    BulkProcessor.Listener bulkProcessorListener = new BulkProcessor.Listener() {
        @Override
        public void beforeBulk(long executionId, BulkRequest request) {
            logger.info("Bulk inserting employee sample data...");
        }

        @Override
        public void afterBulk(long executionId, BulkRequest request,
                              BulkResponse response) {
            if (response.hasFailures()) {
                logger.info("Response found failures: " + response.buildFailureMessage());
                throw new RuntimeException("Response found failures: " + response.buildFailureMessage());
            }
            logger.info("Bulk indexing succeeded!");
        }

        @Override
        public void afterBulk(long executionId, BulkRequest request,
                              Throwable failure) {
            logger.error("Indexing failed: " + failure.getMessage());
            throw new RuntimeException("BulkResponse show failures: " + failure.getMessage());
        }
    };

    public List<Employee> searchEmployees(Map<String, String> filterMap) throws IOException {
        logger.info("Searching employees with filter: " + filterMap.entrySet().stream().findFirst().get());
        final List<Employee> employees = new ArrayList<>();
        final SearchRequest searchRequest = new SearchRequest(EMPLOYEE_SAMPLE);
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(createQueryBuilder(filterMap));
        searchSourceBuilder.size(50);
        searchRequest.source(searchSourceBuilder);
        final SearchResponse searchResponse = restClient.search(searchRequest, RequestOptions.DEFAULT);
        for (final SearchHit searchHit : searchResponse.getHits().getHits()) {
            final Employee user = new ObjectMapper().readValue(searchHit.getSourceAsString(), Employee.class);
            employees.add(user);
        }
        logger.info("Search completed with " + searchResponse.getHits().getHits().length + " hits found!");
        return employees;
    }

    private QueryBuilder createQueryBuilder(Map<String, String> filterMap) {
        if (filterMap.isEmpty()) {
            return QueryBuilders.matchAllQuery();
        }
        return QueryBuilders.matchQuery(filterMap.keySet().stream().findFirst().get(), filterMap.entrySet().stream().findFirst().get().getValue());
    }

    public void bulkInsert() {
        if (!template.indexExists(INDEX_NAME)) {
            logger.info(INDEX_NAME + " not found! Creating index...");
            template.createIndex(INDEX_NAME);
        }

        final List<Employee> employees = generateEmployeesWithSkillsSample();
        final BulkProcessor bulkProcessor = BulkProcessor.builder(
                (request, bulkListener) ->
                        restClient.bulkAsync(request, RequestOptions.DEFAULT, bulkListener),
                bulkProcessorListener)
                .setBulkActions(10000)
                .setFlushInterval(TimeValue.timeValueSeconds(5))
                .build();
        employees.forEach(employee -> {
            try {
                final IndexRequest indexRequest = new IndexRequest(INDEX_NAME, INDEX_TYPE, employee.getId()).source(objectMapper.writeValueAsString(employee), XContentType.JSON);
                bulkProcessor.add(indexRequest);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
        bulkProcessor.close();
    }

    private List<Employee> generateEmployeesWithSkillsSample() {
        final List<Employee> employees = new ArrayList<>();
        for (int employeeIndex = 0; employeeIndex < 1000; employeeIndex++) {
            final Random random = new Random();
            final String employeeId = String.valueOf(random.nextInt(500000));
            final Employee newEmployee = new Employee();
            newEmployee.setId(employeeId);
            newEmployee.setName(namesArray[random.nextInt(namesArray.length)]);
            newEmployee.setAge(random.nextInt(100));
            newEmployee.setAgency(agenciesArray[random.nextInt(agenciesArray.length)]);
            final List<Skill> skills = new ArrayList<>();
            for (int skillIndex = 0; skillIndex < 5; skillIndex++) {
                final String skillId = String.valueOf(random.nextInt(500000));
                final Skill newSkill = new Skill();
                newSkill.setId(skillId);
                newSkill.setTechnology(technologiesArray[random.nextInt(technologiesArray.length)]);
                newSkill.setProficiency(proficiencyArray[random.nextInt(proficiencyArray.length)]);
                newSkill.setMain(employeeIndex % 3 == 0 ? true : false);
                skills.add(newSkill);
            }
            newEmployee.setSkills(skills);
            employees.add(newEmployee);
        }
        return employees;
    }

}

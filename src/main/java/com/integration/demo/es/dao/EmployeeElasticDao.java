package com.integration.demo.es.dao;

import com.integration.demo.es.document.Employee;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * This is the employee elastic dao interface
 */
public interface EmployeeElasticDao {

    List<Employee> searchEmployees(Map<String, String> filtersMap) throws IOException;

    void bulkInsert();
}

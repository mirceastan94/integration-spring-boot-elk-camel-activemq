package com.integration.demo.es.service;

import com.google.common.collect.ImmutableMap;
import com.integration.demo.es.dao.EmployeeElasticDao;
import com.integration.demo.es.document.Employee;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * This is the employee elastic service implementation
 */
@Service
public class EmployeeElasticServiceImpl implements EmployeeElasticService {

    @Autowired
    private RestHighLevelClient restClient;

    @Autowired
    private EmployeeElasticDao employeeElasticDao;

    public List<Employee> readAllEmployees() throws IOException {
        return employeeElasticDao.searchEmployees(new HashMap<>());
    }

    public List<Employee> readEmployeesFromAgency(String searchedParameter, String searchedValue) throws IOException {
        return employeeElasticDao.searchEmployees(ImmutableMap.of(searchedParameter, searchedValue));
    }

    public void postEmployeesSample() {
        employeeElasticDao.bulkInsert();
    }

}

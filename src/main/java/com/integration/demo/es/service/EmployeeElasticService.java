package com.integration.demo.es.service;

import com.integration.demo.es.document.Employee;

import java.io.IOException;
import java.util.List;

/**
 * This is the employee elastic service interface
 */
public interface EmployeeElasticService {

    List<Employee> readAllEmployees() throws IOException;

    void postEmployeesSample();

}

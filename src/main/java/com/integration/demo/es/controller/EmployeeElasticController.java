package com.integration.demo.es.controller;

import com.integration.demo.es.service.EmployeeElasticServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * This is the REST controller class for the employee elastic operations
 */
@RestController
@RequestMapping("/es/employees")
public class EmployeeElasticController {

    @Autowired
    private EmployeeElasticServiceImpl employeeElasticService;

    @GetMapping("")
    public ResponseEntity<?> readAllEmployees() throws IOException {
        return new ResponseEntity<>(employeeElasticService.readAllEmployees(), HttpStatus.OK);
    }

    @GetMapping("/{parameter}/{value}")
    public ResponseEntity<?> readEmployeesByParameter(@PathVariable(value = "parameter") String searchedParameter,
                                                      @PathVariable(value = "value") String searchedValue) throws IOException {
        return new ResponseEntity<>(employeeElasticService.readEmployeesFromAgency(searchedParameter, searchedValue), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<?> indexEmployees() throws IOException {
        employeeElasticService.postEmployeesSample();
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

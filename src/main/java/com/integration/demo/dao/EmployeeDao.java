package com.integration.demo.dao;

import com.integration.demo.jms.jaxb.EmployeeJAXB;

/**
 * This is the employee dao interface
 */
public interface EmployeeDao {

    void insertOrUpdate(EmployeeJAXB employeeElem);

}

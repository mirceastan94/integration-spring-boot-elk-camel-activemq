package com.integration.demo.dao;

import com.integration.demo.entity.Employee;
import com.integration.demo.entity.EmployeeSkill;
import com.integration.demo.entity.Skill;
import com.integration.demo.entity.spec.Proficiency;
import com.integration.demo.jms.jaxb.EmployeeJAXB;
import com.integration.demo.jms.jaxb.SkillJAXB;
import com.integration.demo.jms.validator.EmployeeValidator;
import com.integration.demo.repository.EmployeeRepository;
import com.integration.demo.repository.EmployeeSkillRepository;
import com.integration.demo.repository.SkillRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static com.integration.demo.adapter.EmployeeAdapter.mapToEmployeeEntity;

/**
 * This is the employee dao implementation class
 */
@Component
public class EmployeeDaoImpl implements EmployeeDao {

    static final Logger logger = LoggerFactory.getLogger(EmployeeValidator.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeSkillRepository employeeSkillRepository;

    @Autowired
    private SkillRepository skillRepository;

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void insertOrUpdate(EmployeeJAXB employeeElem) {
        logger.info("Upserting employee...");
        final Employee employee = employeeRepository.findByEmail(employeeElem.getEmail()).orElse(new Employee());
        mapToEmployeeEntity(employeeElem, employee);
        employeeRepository.save(employee);
        logger.info("Upserted initial information of employee with ID: " + employee.getId());
        final List<SkillJAXB> skillsElem = employeeElem.getSkills();
        if (skillsElem != null && !skillsElem.isEmpty()) {
            final List<EmployeeSkill> employeeSkills = employee.getSkills();
            if (employeeSkills.isEmpty()) {
                skillsElem.forEach(skillJAXB -> {
                    Optional<Skill> optSkill = skillRepository.findByTechnology(skillJAXB.getTechnology());
                    if (!optSkill.isPresent()) {
                        optSkill = Optional.of(skillRepository.save(new Skill(skillJAXB.getTechnology())));
                    }
                    employeeSkillRepository.save(new EmployeeSkill(employee, optSkill.get(),
                            Proficiency.valueOf(skillJAXB.getProficiency().name()), skillJAXB.isMain()));
                });
            } else {
                skillsElem.forEach(skillJAXB -> {
                    Optional<Skill> optSkill = skillRepository.findByTechnology(skillJAXB.getTechnology());
                    if (!optSkill.isPresent()) {
                        optSkill = Optional.of(skillRepository.save(new Skill(skillJAXB.getTechnology())));
                    }
                    final Skill skill = optSkill.get();
                    final EmployeeSkill employeeSkill = employeeSkillRepository.findByEmployeeAndSkill(employee, skill)
                            .orElse(new EmployeeSkill());
                    employeeSkill.setEmployee(employee);
                    employeeSkill.setSkill(skill);
                    employeeSkill.setProficiency(Proficiency.valueOf(skillJAXB.getProficiency().name()));
                    employeeSkill.setMain(skillJAXB.isMain());
                    employeeSkillRepository.save(employeeSkill);
                    skill.getEmployee().add(employeeSkill);
                    skillRepository.save(skill);
                });
            }
            logger.info("Upserted skills for employee with ID: " + employee.getId());
        } else {
            logger.info("No skills to upsert for employee with ID: " + employee.getId());
        }
    }
}

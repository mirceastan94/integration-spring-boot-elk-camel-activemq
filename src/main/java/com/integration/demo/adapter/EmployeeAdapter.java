package com.integration.demo.adapter;

import com.integration.demo.entity.Employee;
import com.integration.demo.entity.Employer;
import com.integration.demo.entity.spec.Agency;
import com.integration.demo.jms.jaxb.EmployeeJAXB;
import com.integration.demo.jms.jaxb.EmployerJAXB;

/**
 * This is the employer adapter class
 */
public class EmployeeAdapter {

    public static void mapToEmployeeEntity(EmployeeJAXB employeeElem, Employee employee) {
        employee.setAge(employeeElem.getAge());
        employee.setAgency(Agency.valueOf(employeeElem.getAgency().name()));
        employee.setEmail(employeeElem.getEmail());
        employee.setName(employeeElem.getName());
        employee.setSalary(employeeElem.getSalary());
    }

}

package com.integration.demo.entity;

import com.integration.demo.entity.spec.Agency;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This is the employee entity class
 */
@Entity
@Table(name = "employees", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"email"})
})
@NoArgsConstructor
@Getter
@Setter
public class Employee {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @NotBlank
    @Size(max = 75)
    private String name;

    @NotBlank
    @Size(max = 75)
    private String email;

    @Min(20)
    private Short age;

    @Min(1)
    private Double salary;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    private Agency agency;

    @OneToMany(mappedBy = "employee")
    private List<EmployeeSkill> skills = new ArrayList<>();

}

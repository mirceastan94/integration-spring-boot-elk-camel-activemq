package com.integration.demo.entity;

import com.integration.demo.entity.spec.Agency;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

/**
 * This is the employer entity class
 */
@Entity
@Table(name = "employers", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "name"
        })
})
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Employer {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @NotBlank
    @Size(max = 50)
    private String name;

    @NotBlank
    @Size(max = 100)
    private String headquarters;

    @Min(1)
    private Integer employees;

    @ElementCollection(targetClass = Agency.class, fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "employer_agencies")
    @Column(name = "agency")
    private Collection<Agency> agencies = new ArrayList<>();

}

package com.integration.demo.entity.spec;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

/**
 * This is the proficiency enum
 */
public enum Proficiency {
    LOW,
    MEDIUM,
    HIGH,
    EXTREME
}

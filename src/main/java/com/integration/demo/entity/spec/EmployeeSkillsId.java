package com.integration.demo.entity.spec;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.UUID;

/**
 * This is the employee_skills join table ids class
 */
@Getter
@Setter
@ToString
public class EmployeeSkillsId implements Serializable {

    private UUID employee;
    private UUID skill;

}

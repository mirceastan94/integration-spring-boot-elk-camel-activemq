package com.integration.demo.entity.spec;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

/**
 * This is the agency enum
 */
public enum Agency {
    BUCHAREST,
    BRASOV,
    IASI,
    CLUJ,
    PARIS,
    FRANKFURT
}

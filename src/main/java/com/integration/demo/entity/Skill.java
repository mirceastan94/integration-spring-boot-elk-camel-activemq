package com.integration.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

/**
 * This is the skill entity class
 */
@Entity
@Table(name = "skills", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "technology"
        })
})
@NoArgsConstructor
@Getter
@Setter
public class Skill {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @NotBlank
    @Size(max = 50)
    private String technology;

    @OneToMany(mappedBy = "skill")
    private List<EmployeeSkill> employee;

    public Skill(String technology) {
        this.technology = technology;
    }

}

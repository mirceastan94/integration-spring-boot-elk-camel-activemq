package com.integration.demo.entity;

import com.integration.demo.entity.spec.EmployeeSkillsId;
import com.integration.demo.entity.spec.Proficiency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * This is the employee skill join table entity class
 */
@Entity
@Table(name = "employee_skills")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@IdClass(EmployeeSkillsId.class)
public class EmployeeSkill {

    @Id
    @ManyToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "id")
    private Employee employee;

    @Id
    @ManyToOne
    @JoinColumn(name = "skill_id", referencedColumnName = "id")
    private Skill skill;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Proficiency proficiency;

    @NotNull
    private boolean main;


}

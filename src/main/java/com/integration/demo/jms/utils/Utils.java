package com.integration.demo.jms.utils;

/**
 * This is the utils class for integration information
 */
public class Utils {


    public static final String EMPLOYERS_XML_PATH = "src/main/resources/static/employers.xml";
    public static final String EMPLOYEES_XML_FILE = "src/main/resources/static/employees.xml";
    public static final String INPUT_QUEUE = "InputQueue";
    public static final String NAME_REGEX = "^[a-zA-Z\\\\s]*$";
    public static final String EMAIL_REGEX = "^(.+)@(.+)$";
    public static final String EMPLOYEE_SAMPLE = "employeesample";

}

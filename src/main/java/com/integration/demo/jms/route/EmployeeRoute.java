package com.integration.demo.jms.route;

import com.integration.demo.jms.jaxb.EmployeeJAXB;
import org.apache.camel.LoggingLevel;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;

/**
 * This is the employee route class
 */
@Component
public class EmployeeRoute extends BaseRouteBuilder {

    static final Logger logger = LoggerFactory.getLogger(EmployeeRoute.class);

    public static final String ROUTE_ID = "employee-route";
    public static final String ROUTE_PATH = "direct:" + ROUTE_ID;

    @Override
    public void configure() throws Exception {
        super.configure();

        final JAXBContext jaxbContext = JAXBContext.newInstance(EmployeeJAXB.class);
        final JaxbDataFormat jaxbDataFormat = new JaxbDataFormat(jaxbContext);

        from(ROUTE_PATH)
                .log(LoggingLevel.INFO, logger, "Message received via: " + ROUTE_PATH)
                .unmarshal(jaxbDataFormat)
                .filter().method("employeeValidator", "isEmployeeValid")
                .process("employeeProcessor")
                .to("{{output.queue}}")
                .log(LoggingLevel.INFO, logger, "Employee persisted in the database!")
                .end();
    }
}

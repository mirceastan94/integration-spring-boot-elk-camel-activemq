package com.integration.demo.jms.route;

import com.integration.demo.jms.jaxb.EmployerJAXB;
import org.apache.camel.LoggingLevel;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;

/**
 * This is the employer route class
 */
@Component
public class EmployerRoute extends BaseRouteBuilder {

    static final Logger logger = LoggerFactory.getLogger(EmployerRoute.class);

    public static final String ROUTE_ID = "employer-route";
    public static final String ROUTE_PATH = "direct:" + ROUTE_ID;

    @Override
    public void configure() throws Exception {
        super.configure();

        final JAXBContext jaxbContext = JAXBContext.newInstance(EmployerJAXB.class);
        final JaxbDataFormat jaxbDataFormat = new JaxbDataFormat(jaxbContext);

        from(ROUTE_PATH)
                .log(LoggingLevel.INFO, logger, "Message received via: " + ROUTE_PATH)
                .unmarshal(jaxbDataFormat)
                .filter().method("employerValidator", "isEmployerValid")
                .process("employerProcessor")
                .to("{{output.queue}}")
                .log(LoggingLevel.INFO, logger, "Employer persisted in the database!")
                .end();
    }
}

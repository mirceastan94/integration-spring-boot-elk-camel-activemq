package com.integration.demo.jms.route;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;

/**
 * This is the base route builder class
 */
public class BaseRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel("mock:dead")
                .maximumRedeliveries(1)
                .redeliveryDelay(1000)
                .retryAttemptedLogLevel(LoggingLevel.WARN));
    }

}

package com.integration.demo.jms.route;

import com.integration.demo.jms.jaxb.Envelope;
import org.apache.camel.LoggingLevel;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;

/**
 * This is the envelope route class
 */
@Component
public class EnvelopeRoute extends BaseRouteBuilder {

    static final Logger logger = LoggerFactory.getLogger(EnvelopeRoute.class);

    @Override
    public void configure() throws Exception {
        super.configure();

        final JAXBContext jaxbContext = JAXBContext.newInstance(Envelope.class);
        final JaxbDataFormat jaxbDataFormat = new JaxbDataFormat(jaxbContext);

        from("{{input.queue}}")
                .log(LoggingLevel.INFO, logger, "Message received via input queue...")
                .unmarshal(jaxbDataFormat)
                .choice()
                .when(header("JMSType").isEqualTo("employee"))
                    .process(exchange -> exchange.getIn().setBody(exchange.getIn().getBody(Envelope.class).getEmployees()))
                    .split((body()))
                    .to(EmployeeRoute.ROUTE_PATH)
                    .log(LoggingLevel.INFO, logger, "Employee(s) sent to route: " + EmployeeRoute.ROUTE_PATH)
                    .endChoice()
                .when(header("JMSType").isEqualTo("employer"))
                    .process(exchange -> exchange.getIn().setBody(exchange.getIn().getBody(Envelope.class).getEmployers()))
                    .split((body()))
                    .to(EmployerRoute.ROUTE_PATH)
                    .log(LoggingLevel.INFO, logger, "Message sent to route: " + EmployerRoute.ROUTE_PATH)
                    .endChoice()
                .otherwise()
                    .to("{{ack.queue}}")
                    .log(LoggingLevel.WARN, logger, "Invalid header type, sent to ack queue!")
                .end();
    }
}

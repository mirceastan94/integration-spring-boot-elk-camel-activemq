package com.integration.demo.jms.processor;

import com.integration.demo.jms.jaxb.EmployeeJAXB;
import com.integration.demo.service.EmployeeService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This is the employee processor class
 */
@Component
public class EmployeeProcessor implements Processor {

    static final Logger logger = LoggerFactory.getLogger(EmployeeProcessor.class);

    @Autowired
    private EmployeeService employeeService;

    @Override
    public void process(Exchange exchange) throws Exception {
        final EmployeeJAXB employee = exchange.getIn().getBody(EmployeeJAXB.class);
        logger.info("Received and persisting employee: " + employee);
        employeeService.integrateEmployee(employee);
        logger.info("Persisted employee: " + employee);
        exchange.getIn().setBody("Persisted employee: " + employee);
    }
}

package com.integration.demo.jms.processor;

import com.integration.demo.jms.jaxb.EmployerJAXB;
import com.integration.demo.jms.route.EmployeeRoute;
import com.integration.demo.service.EmployerService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This is the employer processor class
 */
@Component
public class EmployerProcessor implements Processor {

    static final Logger logger = LoggerFactory.getLogger(EmployerProcessor.class);

    @Autowired
    private EmployerService employerService;

    @Override
    public void process(Exchange exchange) throws Exception {
        final EmployerJAXB employer = exchange.getIn().getBody(EmployerJAXB.class);
        logger.info("Received and persisting employer: " + employer);
        employerService.integrateEmployer(employer);
        logger.info("Persisted employer: " + employer);
        exchange.getIn().setBody("Persisted employer: " + employer);
    }
}

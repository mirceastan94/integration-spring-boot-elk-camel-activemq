package com.integration.demo.jms.validator;

import com.integration.demo.jms.jaxb.EmployerJAXB;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * This is the employer input validation class
 */
@Component
public class EmployerValidator {

    static final Logger logger = LoggerFactory.getLogger(EmployeeValidator.class);

    public boolean isEmployerValid(Exchange exchange) {
        final EmployerJAXB inEmployer = exchange.getIn().getBody(EmployerJAXB.class);
        if (inEmployer.getEmployees() < 5
                || (inEmployer.getHeadquarters() != null && inEmployer.getHeadquarters().isBlank())
                || (inEmployer.getHeadquarters() != null && inEmployer.getName().isBlank())) {
            logger.error("Employer invalid: " + inEmployer);
            return false;
        }
        return true;
    }

}

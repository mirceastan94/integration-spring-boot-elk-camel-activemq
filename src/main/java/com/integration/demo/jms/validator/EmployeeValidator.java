package com.integration.demo.jms.validator;

import com.integration.demo.jms.jaxb.EmployeeJAXB;
import com.integration.demo.jms.processor.EmployeeProcessor;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static com.integration.demo.jms.utils.Utils.EMAIL_REGEX;
import static com.integration.demo.jms.utils.Utils.NAME_REGEX;

/**
 * This is the employee input validation class
 */
@Component
public class EmployeeValidator {

    static final Logger logger = LoggerFactory.getLogger(EmployeeValidator.class);

    public boolean isEmployeeValid(Exchange exchange) {
        final EmployeeJAXB inEmployee = exchange.getIn().getBody(EmployeeJAXB.class);
        if (inEmployee.getSkills().isEmpty()
                || (inEmployee.getAge() < 18)
                || !inEmployee.getName().matches(NAME_REGEX)
                || !inEmployee.getEmail().matches(EMAIL_REGEX)
                || inEmployee.getSalary() < 2230) {
            logger.error("Employee invalid: " + inEmployee);
            return false;
        }
        return true;
    }

}

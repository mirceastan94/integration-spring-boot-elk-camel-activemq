package com.integration.demo.jms.jaxb;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * This is the envelope JAXB class
 */
@XmlRootElement(name = "envelope")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"employees", "employers"})
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Envelope {

    @XmlElementWrapper(name="employees")
    @XmlElement(name="employee")
    private List<EmployeeJAXB> employees;

    @XmlElementWrapper(name="employers")
    @XmlElement(name = "employer")
    private List<EmployerJAXB> employers;

}

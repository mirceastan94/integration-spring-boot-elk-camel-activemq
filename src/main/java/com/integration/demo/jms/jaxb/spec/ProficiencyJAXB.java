package com.integration.demo.jms.jaxb.spec;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

/**
 * This is the proficiency JAXB enum
 */
@XmlEnum
public enum ProficiencyJAXB {
    @XmlEnumValue("Low") LOW,
    @XmlEnumValue("Medium") MEDIUM,
    @XmlEnumValue("High") HIGH,
    @XmlEnumValue("Extreme") EXTREME;
}

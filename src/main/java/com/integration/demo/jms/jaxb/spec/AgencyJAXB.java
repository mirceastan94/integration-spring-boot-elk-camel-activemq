package com.integration.demo.jms.jaxb.spec;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

/**
 * This is the agency JAXB enum
 */
@XmlEnum
public enum AgencyJAXB {
    @XmlEnumValue("Bucharest") BUCHAREST,
    @XmlEnumValue("Brasov") BRASOV,
    @XmlEnumValue("Iasi") IASI,
    @XmlEnumValue("Cluj") CLUJ,
    @XmlEnumValue("Paris") PARIS,
    @XmlEnumValue("Frankfurt") FRANKFURT
}

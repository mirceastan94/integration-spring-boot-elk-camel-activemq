package com.integration.demo.jms.jaxb;

import com.integration.demo.jms.jaxb.spec.ProficiencyJAXB;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;

/**
 * This is the skill JAXB class
 */
@XmlRootElement(name = "skill")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"technology", "proficiency", "main"})
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SkillJAXB {

    @XmlElement(required = true)
    private String technology;

    @XmlElement(required = true)
    private ProficiencyJAXB proficiency;

    @XmlElement(required = true)
    private boolean main;

}

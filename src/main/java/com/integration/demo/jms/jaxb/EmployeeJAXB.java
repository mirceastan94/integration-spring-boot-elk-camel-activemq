package com.integration.demo.jms.jaxb;

import com.integration.demo.jms.jaxb.spec.AgencyJAXB;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * This is the employee JAXB class
 */
@XmlRootElement(name = "employee")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"name", "email", "age", "salary", "agency", "skills"})
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class EmployeeJAXB {

    @XmlElement(required = true)
    private String name;

    @XmlElement(required = true)
    private String email;

    @XmlElement(required = true)
    private short age;

    @XmlElement(required = true)
    private Double salary;

    @XmlElement(required = true)
    private AgencyJAXB agency;

    @XmlElementWrapper(name="skills")
    @XmlElement(name="skill", required = true)
    private List<SkillJAXB> skills;
}

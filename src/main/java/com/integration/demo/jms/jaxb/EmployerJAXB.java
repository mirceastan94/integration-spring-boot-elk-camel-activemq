package com.integration.demo.jms.jaxb;

import com.integration.demo.jms.jaxb.spec.AgencyJAXB;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * This is the employer JAXB class
 */
@XmlRootElement(name = "employer")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"name", "headquarters", "employees", "agencies"})
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class EmployerJAXB {

    @XmlElement(required = true)
    private String name;

    @XmlElement(required = true)
    private String headquarters;

    @XmlElement(required = true)
    private Integer employees;

    @XmlElementWrapper(name = "agencies")
    @XmlElement(name = "agency", required = true)
    private List<AgencyJAXB> agencies;
}